package ru.tsc.denisturovsky.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

}
