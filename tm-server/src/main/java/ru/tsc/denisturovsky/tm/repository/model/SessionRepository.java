package ru.tsc.denisturovsky.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.repository.model.ISessionRepository;
import ru.tsc.denisturovsky.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}
